﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using Microsoft.Win32;


namespace IM
{
    /// <summary>
    /// 消息格式
    /// 好友消息 Friend##ip:port#name##内容
    /// 下线请求 Offline##ip:port#name#0##
    /// 请求返回数据库在线好友 Read##ip:port#name#null##
    /// 请求发送文件 File##ip:port#name##send
    /// 接收发送文件请求  File##ip:port#name##accept
    /// 拒绝发送文件请求  File##ip:port#name##cancel
    /// MainWindow.xaml 的交互逻辑
    /// 实现p2p的内网即时通讯
    /// v1.0
    /// 可通过ip地址与端口号向其他计算机发送信息
    /// 随时监听其他计算机发来的信息
    /// 成功接收到其他计算机的信息时，将其ip与port添加到好友列表
    /// 允许手工添加好友
    /// 可以群发消息
    /// v2.0
    /// 增加用户名密码注册登录功能
    /// 删掉通过ip与port添加好友
    /// 增加陌生人数据
    /// 可右键陌生人数据添加为好友
    /// 可右键好友数据添加为陌生人
    /// v3.0
    /// 保存好友信息
    /// 保存聊天记录
    /// 读取好友信息，读取聊天记录
    /// v4,0
    /// </summary>
    public partial class MainWindow : Window
    {
        static int MyPort = 1499;  //本机端口号
        IPAddress MyIPAdress = null;  //本机ip地址
        TcpListener tcpListener = null;  //监听
        public string IPAndPort;  //本机ip与端口号
        private string sendfilename = null;
        private Thread Listenerthread;
        private delegate void OneArgDelegate(string arg);
        private delegate void SetList(FriendIPAndPort arg);
        private delegate void ReadDataF(TcpClient tcpClient);
        private delegate void ShowState(string arg);
        private delegate void UpdataUser(string infor);
        private delegate void SetStranger(Stranger arg);
        private delegate void ReceiveMsg(string infor);
        private delegate void OfflineF(string infor);
        private delegate void RemoveItemF(FriendIPAndPort i);
        private delegate void RemoveItemF1(Stranger i);
        private delegate void OnlineF();
        private delegate void ReadchatF();
        private delegate void FileF(string infor,TcpClient tcpClient);
        private delegate void SendfileF(string ip, int port);
        private delegate void CancelFilebuttonF(object sender, RoutedEventArgs e);

        Load_window load;

        //自定义类--建立发送信息连接所需要的信息（客户端-流（内容）-服务端）
        public class StateObject
        {
            public TcpClient tcpClient = null;  //tcp客户端
            public NetworkStream networkStream = null;  //发送信息流
            public byte[] buffer;  //发送信息-字节序列
            public string friengIPAndPort = null;  //好友ip与端口号
        }

        //与FriendListview进行数据绑定
        public struct FriendIPAndPort
        {
            public string friendIP
            {
                get; set;
            }
            public string friendPort
            {
                get; set;
            }
            public string friendName
            {
                get;  set;
            }
            public string friendState
            {
                get;  set;
            }
        }
        public class FriendIPAndPorts : ObservableCollection<FriendIPAndPort> { };
        FriendIPAndPorts myFriendIPAndPort = new FriendIPAndPorts();  //列表的好友数据

        //与StrangerListview进行数据绑定
        public struct Stranger
        {
            public string strangerIP
            {
                get; set;
            }
            public string strangerPort
            {
                get; set;
            }
            public string strangerName
            {
                get; set;
            }
            public string strangerState
            {
                get; set;
            }
        }
        public class Strangers : ObservableCollection<Stranger> { };
        Strangers myStrangers = new Strangers();  //列表的陌生人数据

        //主窗口构造函数
        public MainWindow()
        {
            InitializeComponent();

            //查找可以的端口
            //筛选获取本机ipv4地址,参考博客：https://bbs.csdn.net/topics/390391867
            //MyIPAdress = (IPAddress)Dns.GetHostAddresses(Dns.GetHostName()).Where(ip => ip.AddressFamily == AddressFamily.InterNetwork).First();
            MyIPAdress = (IPAddress)Dns.GetHostAddresses(Dns.GetHostName()).Last();
            MyPort++;
            for (int i = 0; i < 51; i++)
            {
                //端口尝试
                try
                {
                    tcpListener = new TcpListener(MyIPAdress, MyPort);
                    tcpListener.Start();
                    break;
                }
                catch
                {
                    MyPort++;
                }
                if (i == 50)
                {
                    MessageBox.Show("无法建立服务器，请检查您的网络连接！");
                    this.Close();
                }
            }
            FrientlistView.ItemsSource = myFriendIPAndPort;
            StrangerlistView.ItemsSource = myStrangers;
            IPAndPort = MyIPAdress.ToString() + ":" + MyPort.ToString();

            Listenerthread = new Thread(new ThreadStart(ListenerthreadMethod));
            Listenerthread.IsBackground = true;
            Listenerthread.Start();
        }

        //线程方法ListenerthreadMethod
        public void ListenerthreadMethod()
        {
            TcpClient tcpClient = null;
            ReadDataF readDataF = new ReadDataF(readRevMsg);
            while (true)
            {
                try
                {
                    //同步阻塞
                    tcpClient = tcpListener.AcceptTcpClient();
                    //异步调用
                    readDataF.BeginInvoke(tcpClient, null, null);
                }
                catch { }
            }
        }

        //从TcpClient对象中读出未知长度的字节数组
        public byte[] ReadFromTcpClient(TcpClient tcpClient)
        {
            List<byte> data = new List<byte>();
            NetworkStream networkStream = null;
            //新建可储存缓冲区长度的字节数组
            byte[] bytes = new byte[tcpClient.ReceiveBufferSize];
            int n = 0;
            try
            {
                networkStream = tcpClient.GetStream();
                if (networkStream.CanRead)
                {
                    do
                    {
                        n = networkStream.Read(bytes, 0, (int)tcpClient.ReceiveBufferSize);
                        //若还未读到末尾
                        if (n == (int)tcpClient.ReceiveBufferSize) data.AddRange(bytes);
                        //读到结尾
                        else if (n != 0)
                        {
                            byte[] bytes1 = new byte[n];
                            for (int i = 0; i < n; i++)
                            {
                                bytes1[i] = bytes[i];
                            }
                            data.AddRange(bytes1);
                        }
                    } while (networkStream.DataAvailable);
                }
                bytes = data.ToArray();
            }
            catch
            {
                MessageBox.Show("读取数据失败！");
                bytes = null;
            }
            finally
            {
                if (networkStream != null) networkStream.Close();
            }
            return bytes;
        }

        //接收到信息后的操作
        public void readRevMsg(TcpClient tcpClient)
        {
            //从流中读取数据并转为string
            byte[] bytes = ReadFromTcpClient(tcpClient);
            UnicodeEncoding ascii = new UnicodeEncoding();

            string s;
            byte[] newA = bytes.Skip(0).Take(10).ToArray();
            s = ascii.GetString(newA);
            if (s == "SFile")
            {
                FileStream fs = null;
                try
                {
                    SaveFileDialog saveFileDialog = new SaveFileDialog();  //建立文件另存为窗口
                    saveFileDialog.Filter = "rar文件(*.rar)|*.rar";
                    if (saveFileDialog.ShowDialog().Value)  //若用户点击了另存为确定按钮
                    {
                        string FileName = saveFileDialog.FileName;
                        fs = new FileStream(FileName, FileMode.Create);
                        fs.Write(bytes, 10, bytes.Length-10);
                    }
                }
                catch
                {
                    MessageBox.Show("写文件失败！");
                }
                finally
                {
                    if (fs != null) fs.Close();
                }
                return;
            }

            s = ascii.GetString(bytes);
            //解析消息
            int i1 = s.IndexOf("##");
            string title = s.Substring(0, i1);  //消息头
            string infor = s.Substring(i1 + 2,s.Length-i1-2);  //发送者信息
            switch (title)
            {
                case "Login":
                    //登录失败
                    if (infor == "0")
                    {
                        string t = "登录失败，可以尝试注册一下！##1";
                        //直接关闭不行，不在同一线程
                        this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new ShowState(load.state), t);
                    }
                    else
                    {
                        string t = "登录成功！##0";
                        //直接关闭不行，不在同一线程
                        this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new ShowState(load.state), t);
                        this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new OnlineF(onlineF));
                        this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new ReadchatF(readchatF));
                    }

                    //LoginF loginF = new LoginF(login);
                    //loginF.BeginInvoke(infor, null, null);
                    tcpClient.Close();
                    break;
                case "Register":
                    if (infor == "0")
                    {
                        string t = "注册失败，换个用户名试试吧！##1";
                        //直接关闭不行，不在同一线程
                        this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new ShowState(load.state), t);
                    }
                    else
                    {
                        string t = "注册成功！尝试登录一下吧！##1";
                        //直接关闭不行，不在同一线程
                        this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new ShowState(load.state), t);
                     }
                    //RegisterF registerF = new RegisterF(register);
                    //registerF.BeginInvoke(infor, null, null);
                    tcpClient.Close();
                    break;
                case "Online":
                    UpdataUser UpdataUserF = new UpdataUser(updataUser);
                    UpdataUserF.BeginInvoke(infor, null, null);
                    break;

                case "Friend":
                    ReceiveMsg ReceiveMsgF = new ReceiveMsg(receiveMsg);
                    ReceiveMsgF.BeginInvoke(infor, null, null);
                    tcpClient.Close();
                    break;
                case "Offline":
                    OfflineF offlineF = new OfflineF(offline);
                    offlineF.BeginInvoke(infor, null, null);
                    tcpClient.Close();
                    break;
                case "File":
                    FileF fileF = new FileF(filetrans);
                    fileF.BeginInvoke(infor,tcpClient, null, null);
                    tcpClient.Close();
                    break;
            }
        }

        //异步更新用户列表
        public void updataUser(string infor)
        {
            int i = 0;
            int i3 = infor.IndexOf("#");
            int i2 = infor.IndexOf(":");
            string userip = infor.Substring(0, i2);
            string userport = infor.Substring(i2 + 1, i3 - i2 - 1);
            string username = infor.Substring(i3 + 1, infor.Length - i3 - 1);
            foreach(FriendIPAndPort f in myFriendIPAndPort)
            {
                if (f.friendName == username) break;
                i++;
            }
            //不是好友，添加到陌生人,不能添加本人
            if (i == myFriendIPAndPort.Count)
            {
                if(username!=load.s_name)
                {
                    Stranger stranger = new Stranger();
                    stranger.strangerIP = userip;
                    stranger.strangerPort = userport;
                    stranger.strangerName = username;
                    stranger.strangerState = "在线";
                    this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new SetStranger(setStrangerF), stranger);
                }
            }
            else
            {
                FriendIPAndPort friendIPAndPort = new FriendIPAndPort();
                friendIPAndPort.friendIP = userip;
                friendIPAndPort.friendPort = userport;
                friendIPAndPort.friendName = username;
                friendIPAndPort.friendState = "在线" + i.ToString();
                this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new SetList(SetListViewSource),friendIPAndPort );
            }
        }

        //陌生人列表添加数据
        private void setStrangerF(Stranger arg)
        {
            myStrangers.Add(arg);
        }

        //好友列表添加好友
        private void SetListViewSource(FriendIPAndPort arg)
        {
            string s = arg.friendState.Substring(2, arg.friendState.Length - 2);
            int i = int.Parse(s);
            //数据库返回信息更新
            arg.friendState = "在线";
            myFriendIPAndPort.Remove(myFriendIPAndPort[i]);
            myFriendIPAndPort.Insert(0, arg);
        }

        //右键陌生人添加为好友
        private void AddFriend(object sender, RoutedEventArgs e)
        {
            Stranger stranger = (Stranger)StrangerlistView.SelectedItem; //选中的陌生人数据
            int i = StrangerlistView.SelectedIndex;
            if (i == -1)
            {
                MessageBox.Show("请选择。。。");
                return;
            }
            FriendIPAndPort friendIPAndPort = new FriendIPAndPort();
            friendIPAndPort.friendIP = stranger.strangerIP;
            friendIPAndPort.friendName = stranger.strangerName;
            friendIPAndPort.friendPort = stranger.strangerPort;
            friendIPAndPort.friendState = stranger.strangerState;

            myStrangers.Remove(stranger);  //陌生人列表中移除
            myFriendIPAndPort.Add(friendIPAndPort);  //好友列表中添加
        }

        //右键好友删除
        private void DeleteFriend(object sender, RoutedEventArgs e)
        {
            int num = FrientlistView.SelectedItems.Count;
            if (num > 1)
            {
                MessageBox.Show("不能同时删掉多个好友。。");
                return;
            }else if (num == 0)
            {
                MessageBox.Show("请选择好友。。。");
                return;
            }
            FriendIPAndPort friendIPAndPort = (FriendIPAndPort)FrientlistView.SelectedItem;  //选中的好友数据
            Stranger stranger = new Stranger();
            stranger.strangerIP = friendIPAndPort.friendIP;
            stranger.strangerName = friendIPAndPort.friendName;
            stranger.strangerPort = friendIPAndPort.friendPort;
            stranger.strangerState = friendIPAndPort.friendState;

            myFriendIPAndPort.Remove(friendIPAndPort); //好友列表中移除
            myStrangers.Add(stranger);  //陌生人列表中添加
        }

        //登录成功显示登录用户信息并尝试读取本地用户好友信息
        private  void onlineF()
        {
            textBlock_User.Text = load.s_name;
            //读取好友信息
            string path = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + load.s_name + "_Friend.txt";
            try
            {
                FileStream fs = new FileStream(path, FileMode.Open);
                StreamReader sr = new StreamReader(fs, Encoding.UTF8);  //以utf-8格式写出
                string s;
                int i1, i2;
                string ip, port, name;
                FriendIPAndPort friendIPAndPort;
                while (!sr.EndOfStream)
                {
                    s = sr.ReadLine();
                    i1 = s.IndexOf("#");
                    i2 = s.IndexOf("#", i1 + 1);
                    ip = s.Substring(0, i1);
                    port = s.Substring(i1 + 1, i2 - i1 - 1);
                    name = s.Substring(i2 + 1, s.Length - i2 - 1);
                    friendIPAndPort = new FriendIPAndPort();
                    //friendIPAndPort.friendIP = ip;
                    //friendIPAndPort.friendPort = port;
                    friendIPAndPort.friendName = name;
                    friendIPAndPort.friendState = "离线";
                    //无需异步，没有对界面控件操作
                    myFriendIPAndPort.Add(friendIPAndPort);
                }
                sr.Close();
                fs.Close();
            }
            catch { MessageBox.Show("读取本地好友失败！"); }
            finally
            {
                //发送信息给服务器，让服务器返回数据库在线用户信息
                string s = "Read##" + IPAndPort + "#" + load.s_name + "#" + "null" + "##";
                UnicodeEncoding ascii = new UnicodeEncoding();
                int n = (ascii.GetBytes(s)).Length;
                byte[] Send = new byte[n];
                Send = ascii.GetBytes(s);
                //与服务器建立tcp连接
                TcpClient tcpClient;
                StateObject stateObject;
                tcpClient = new TcpClient();
                stateObject = new StateObject();
                stateObject.tcpClient = tcpClient;
                stateObject.buffer = Send;
                stateObject.friengIPAndPort = load.s_ip + ":" + load.s_port;
                int p = int.Parse(load.s_port);
                tcpClient.BeginConnect(load.s_ip, p, new AsyncCallback(SendCallbackF), stateObject);
            }
            

        }

        //异步读取聊天记录
        private void readchatF()
        {
            //读取好友信息
            string path = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + load.s_name + "_Chat.txt";
            try
            {
                FileStream fs = new FileStream(path, FileMode.Open);
                StreamReader sr = new StreamReader(fs, Encoding.UTF8);  //以utf-8格式写出
                string s;
                while (!sr.EndOfStream)
                {
                    s = sr.ReadLine();
                    FriendlistBox.Items.Add(s);
                    //滚动条滑到最下
                    FriendlistBox.ScrollIntoView(FriendlistBox.Items[FriendlistBox.Items.Count - 1]);
                }
                sr.Close();
                fs.Close();
            }
            catch { MessageBox.Show("读取本地聊天记录失败！"); }
        }


        //异步接收好友信息
        void receiveMsg(string infor)
        {
            int i1 = infor.IndexOf("##");
            int i2 = infor.IndexOf("#");
            string msg = infor.Substring(i1 + 2, infor.Length - i1 - 2);  //信息内容
            string name = infor.Substring(i2 + 1, i1 - i2 - 1);
            string text = name + "：   " + msg;
            this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new OneArgDelegate(SetFriendListBox), text);
        }

        //聊天界面添加记录
        private void SetFriendListBox(string text)
        {
            FriendlistBox.Items.Add(text);
            //滚动条滑到最下
            FriendlistBox.ScrollIntoView(FriendlistBox.Items[FriendlistBox.Items.Count - 1]);
        }

        //异步更新下线好友数据
         void offline(string infor)
        {
            int i = 0;
            foreach (FriendIPAndPort f in myFriendIPAndPort)
            {
                if (f.friendName == infor) break;
                i++;
            }
            if (i < myFriendIPAndPort.Count) this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new RemoveItemF(removeItemF), myFriendIPAndPort[i]);
            else
            {
                i = 0;
                foreach (Stranger f in myStrangers)
                {
                    if (f.strangerName == infor) break;
                    i++;
                }
                if (i<myStrangers.Count) this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new RemoveItemF1(removeItemF1), myStrangers[i]);
            }
        }

        private void removeItemF(FriendIPAndPort i)
        {
            myFriendIPAndPort.Remove(i);
        }
        private void removeItemF1(Stranger i)
        {
            myStrangers.Remove(i);
        }


        //发送按钮点击事件--发送信息
        private void SendMesbutton_Click(object sender, RoutedEventArgs e)
        {
            string s;
            if (SendMestextBox.Text == "")
            {
                MessageBox.Show("发送消息不能为空！");
                return;
            }
            if (FrientlistView.SelectedItems.Count == 0)
            {
                MessageBox.Show("请选择好友！");
                return;
            }
            //获取选中的好友
            FriendIPAndPort[] myIPAndPort = new FriendIPAndPort[FrientlistView.SelectedItems.Count];
            for(int i = 0; i < myIPAndPort.Length; i++)
            {
                myIPAndPort[i] = (FriendIPAndPort)FrientlistView.SelectedItems[i];
            }
            //将要发送的string转为字节序列
            //发送文件
            if (SendMestextBox.IsReadOnly)
            {
                s = "File##" + IPAndPort + "#" + load.s_name + "##" + "send";
            }
            //发送文本信息
            else
            {
                s = "Friend##" + IPAndPort + "#" + load.s_name + "##" + SendMestextBox.Text;
                FriendlistBox.Items.Add("我:     " + SendMestextBox.Text);
                FriendlistBox.ScrollIntoView(FriendlistBox.Items[FriendlistBox.Items.Count - 1]);
                SendMestextBox.Clear();
            }
            UnicodeEncoding ascii = new UnicodeEncoding();
            int n = (ascii.GetBytes(s)).Length;
            byte[] Send = new byte[n];
            Send = ascii.GetBytes(s);
            //对选中好友循环建立tcp连接
            string ip = null;
            int port = 0;
            TcpClient tcpClient;
            StateObject stateObject;
            for(int i = 0; i < myIPAndPort.Length; i++)
            {
                try
                {
                    tcpClient = new TcpClient();
                    stateObject = new StateObject();
                    stateObject.tcpClient = tcpClient;
                    stateObject.buffer = Send;
                    ip = myIPAndPort[i].friendIP;
                    port = int.Parse(myIPAndPort[i].friendPort);
                    stateObject.friengIPAndPort = myIPAndPort[i].friendName;
                    tcpClient.BeginConnect(ip, port, new AsyncCallback(SendCallbackF), stateObject);
                }
                catch
                {
                    MessageBox.Show("对方接收消息失败，请选择在线好友！");
                }
            }
        }

        //回调函数SendCallbackF--
        public void SendCallbackF(IAsyncResult ar)
        {
            StateObject stateObject = (StateObject)ar.AsyncState;
            TcpClient tcpClient = stateObject.tcpClient;
            NetworkStream networkStream = null;
            //尝试写数据入流
            try
            {
                tcpClient.EndConnect(ar);
                networkStream = tcpClient.GetStream();
                //把数据写入networkStream
                if (networkStream.CanWrite) networkStream.Write(stateObject.buffer, 0, stateObject.buffer.Length);
                else MessageBox.Show("发送到" + stateObject.friengIPAndPort + "失败！");
            }
            catch
            {
                MessageBox.Show("发送到" + stateObject.friengIPAndPort + "失败！");
            }
            finally
            {
                if (networkStream != null) networkStream.Close();
                tcpClient.Close();
            }
        }


        //主窗口加载完成后
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //登录窗口
            load = new Load_window(IPAndPort);
            load.ShowDialog();
        }

        //退出窗口前
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //若无用户登录
            if (textBlock_User.Text == "")
            {
                return;
            }

            //发送下线信息到服务器
            string s = "Offline##" + IPAndPort + "#" + load.s_name + "#0##";
            UnicodeEncoding ascii = new UnicodeEncoding();
            int n = (ascii.GetBytes(s)).Length;
            byte[] Send = new byte[n];
            Send = ascii.GetBytes(s);
            //与服务器建立tcp连接
            TcpClient tcpClient;
            StateObject stateObject;
            tcpClient = new TcpClient();
            stateObject = new StateObject();
            stateObject.tcpClient = tcpClient;
            stateObject.buffer = Send;
            stateObject.friengIPAndPort = "服务器的下线消息";
            int p = int.Parse(load.s_port);
            tcpClient.BeginConnect(load.s_ip, p, new AsyncCallback(SendCallbackF), stateObject);

            //保存聊天记录
            //参考https://www.jb51.net/article/64582.htm
            string path = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + load.s_name + "_Chat.txt";
            FileStream fs = new FileStream(path, FileMode.Create);
            StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);  //以utf-8格式写出
            for(int i = 0; i < FriendlistBox.Items.Count; i++)
            {
                sw.WriteLine(FriendlistBox.Items[i].ToString());
            }
            sw.Flush();  //清空缓冲区
            sw.Close();
            fs.Close();

            //保存好友列表
            path = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + load.s_name + "_Friend.txt";
            fs = new FileStream(path, FileMode.Create);
            sw = new StreamWriter(fs, Encoding.UTF8);  //以utf-8格式写出
            string s1;
            for (int i = 0; i < myFriendIPAndPort.Count; i++)
            {
                s1 = myFriendIPAndPort[i].friendIP + "#" + myFriendIPAndPort[i].friendPort + "#" + myFriendIPAndPort[i].friendName;
                sw.WriteLine(s1);
            }
            sw.Flush();  //清空缓冲区
            sw.Close();
            fs.Close();
        }

        //点击发送文件按钮,选择要发送的rar文件
        private void SendFilebutton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "rar文件(*.rar)|*.rar";
            openFileDialog1.InitialDirectory = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            if (openFileDialog1.ShowDialog().Value)
            {
                string FileName = openFileDialog1.FileName;
                String fileExtension = System.IO.Path.GetExtension(FileName).ToUpper();
                if (fileExtension == ".RAR")
                {
                    SendMestextBox.Text = FileName;
                    sendfilename = FileName;
                    SendMestextBox.IsReadOnly = true;  //只读
                    CancelFilebutton.Visibility = Visibility.Visible;
                    SendFilebutton.Visibility = Visibility.Hidden;
                }
            }

        }

        //取消发送按钮点击按钮
        private void CancelFilebutton_Click(object sender, RoutedEventArgs e)
        {
            SendMestextBox.Clear();
            SendMestextBox.IsReadOnly = false;
            SendFilebutton.Visibility = Visibility.Visible;
            CancelFilebutton.Visibility = Visibility.Hidden;
        }

        //异步处理文件传输
        private void filetrans(string infor,TcpClient tcpClient1)
        {
            int i1 = infor.IndexOf("##");
            string content = infor.Substring(i1 + 2, infor.Length - i1 - 2);  //内容
            int i2 = infor.IndexOf("#");
            string iport = infor.Substring(0, i2);  //ipandport
            string name = infor.Substring(i2 + 1, i1 - i2 - 1);  //用户名
            int i3 = iport.IndexOf(":");
            string ip = iport.Substring(0, i3);
            int port = int.Parse(iport.Substring(i3 + 1, iport.Length - i3 - 1));

            switch (content)
            {
                case "send":
                    string s;
                    MessageBoxResult result = MessageBox.Show(name+"请求给你发来一个神秘文件\n是否接受？","提示",MessageBoxButton.YesNo,MessageBoxImage.Question);
                    if (result == MessageBoxResult.No)
                    {
                        s = "File##" + IPAndPort + "#" + load.s_name + "##" + "cancel";
                    }
                    else
                    {
                        s = "File##" + IPAndPort + "#" + load.s_name + "##" + "accept";
                    }
                    UnicodeEncoding ascii = new UnicodeEncoding();
                    int n = (ascii.GetBytes(s)).Length;
                    byte[] Send = new byte[n];
                    Send = ascii.GetBytes(s);
                    //与服务器建立tcp连接
                    TcpClient tcpClient;
                    StateObject stateObject;
                    tcpClient = new TcpClient();
                    stateObject = new StateObject();
                    stateObject.tcpClient = tcpClient;
                    stateObject.buffer = Send;
                    stateObject.friengIPAndPort = ip + ":" + port.ToString();
                    tcpClient.BeginConnect(ip, port, new AsyncCallback(SendCallbackF), stateObject);
                    tcpClient1.Close();
                    break;
                case "cancel":
                    MessageBox.Show(name + "拒绝了你的文件发送请求！");
                    this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new CancelFilebuttonF(CancelFilebutton_Click), null,null);
                    break;
                case "accept":
                    this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new CancelFilebuttonF(CancelFilebutton_Click), null,null);
                    SendfileF sendfileF = new SendfileF(sendfile);
                    sendfileF.BeginInvoke(ip,port, null, null);
                    break;
                
            }
        }


        //异步发文件
        private void sendfile(string ip,int port)
        {
            TcpClient tcpClient = null;
            //TcpListener tcpListener = null;
            FileStream fs = new FileStream(sendfilename, FileMode.Open);
            byte[] data = new byte[fs.Length+10];
            fs.Read(data, 10, (int)fs.Length);    //将文件读到字节数组
            fs.Close();
            UnicodeEncoding ascii = new UnicodeEncoding();
            int n = (ascii.GetBytes("SFile")).Length;
            byte[] Send = new byte[n];
            Send = ascii.GetBytes("SFile");
            for (int i = 0; i < n; i++) data[i] = Send[i];


            StateObject stateObject;
            //与服务器建立tcp连接
            tcpClient = new TcpClient();
            stateObject = new StateObject();
            stateObject.tcpClient = tcpClient;
            stateObject.buffer = data;
            stateObject.friengIPAndPort = ip + ":" + port.ToString();
            tcpClient.BeginConnect(ip, port, new AsyncCallback(SendCallbackF), stateObject);

        }
        //回调函数
        public void DownloadCallback1(IAsyncResult ar){
            NetworkStream networkStream = (NetworkStream)ar.AsyncState;
            try
            {
                networkStream.EndWrite(ar);
            }
            catch
            {
                MessageBox.Show("写流文件失败！");
            }
            finally
            {
                if (networkStream != null) networkStream.Close();
            }
        }

    };
}
