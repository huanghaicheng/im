﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace IM
{
    /// <summary>
    /// Load_window.xaml 的交互逻辑
    /// 消息格式
    /// 登录 Login##ip:port#name#password##
    /// 注册 Register##ip:port#name#password##
    /// </summary>
    public partial class Load_window : Window
    {
        string IPAndPort;  //本机IP和port
        public string s_ip;  //服务器ip与port
        public string s_port;
        public string s_name;  //用户名
        public Load_window(string s)
        {
            InitializeComponent();
            IPAndPort = s;
        }

        public class StateObject
        {
            public TcpClient tcpClient = null;  //tcp客户端
            public NetworkStream networkStream = null;  //发送信息流
            public byte[] buffer;  //发送信息-字节序列
            public string name = null;  //用户信息
        }

        //输入限制
        private bool limit()
        {
            string ip = IPtextBox.Text;
            string port = PorttextBox.Text;
            string name = NametextBox.Text;
            string password = pwBox.Password;
            if (ip == "" || port == "" || name == "" || password == "")
            {
                MessageBox.Show("请完整输入！");
                return false;
            }
            int i1 = name.IndexOf("#");
            int i2 = password.IndexOf("#");
            if (i1 != -1 || i2 != -1)
            {
                MessageBox.Show("用户名或密码中不能带有字符#！");
                return false;
            }
            return true;
        }

        //点击登录按钮
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            infortextBlock.Text = "";

            if (!limit()) return;

            string ip = IPtextBox.Text;
            string port = PorttextBox.Text;
            string name = NametextBox.Text;
            string password = pwBox.Password;
            infortextBlock.Text = "正在尝试登录...";

            //将要发送的string转为字节序列
            //登录请求
            string s = "Login##" + IPAndPort + "#" + name + "#" + password + "##";
            UnicodeEncoding ascii = new UnicodeEncoding();
            int n = (ascii.GetBytes(s)).Length;
            byte[] Send = new byte[n];
            Send = ascii.GetBytes(s);
            //与服务器建立tcp连接
            TcpClient tcpClient;
            StateObject stateObject;
            tcpClient = new TcpClient();
            stateObject = new StateObject();
            stateObject.tcpClient = tcpClient;
            stateObject.buffer = Send;
            int p = int.Parse(port);
            tcpClient.BeginConnect(ip, p, new AsyncCallback(SendCallbackF), stateObject);

            s_ip = ip;
            s_port = port;
            s_name = name;
        }

        //回调函数SendCallbackF--
        public void SendCallbackF(IAsyncResult ar)
        {
            StateObject stateObject = (StateObject)ar.AsyncState;
            TcpClient tcpClient = stateObject.tcpClient;
            NetworkStream networkStream = null;
            //尝试写数据入流
            try
            {
                tcpClient.EndConnect(ar);
                networkStream = tcpClient.GetStream();
                //把数据写入networkStream
                if (networkStream.CanWrite) networkStream.Write(stateObject.buffer, 0, stateObject.buffer.Length);
                else MessageBox.Show("发送到服务器失败！");
            }
            catch
            {
                MessageBox.Show("发送到服务器失败！");
            }
            finally
            {
                if (networkStream != null) networkStream.Close();
                tcpClient.Close();
            }
        }

        //点击注册按钮
        private void button2_Click(object sender, RoutedEventArgs e)
        {
            infortextBlock.Text = "";
            if (!limit()) return;

            string ip = IPtextBox.Text;
            string port = PorttextBox.Text;
            string name = NametextBox.Text;
            string password = pwBox.Password;
            infortextBlock.Text = "正在尝试注册...";

            //将要发送的string转为字节序列
            //登录请求
            string s = "Register##" + IPAndPort + "#" + name + "#" + password + "##";
            UnicodeEncoding ascii = new UnicodeEncoding();
            int n = (ascii.GetBytes(s)).Length;
            byte[] Send = new byte[n];
            Send = ascii.GetBytes(s);
            //与服务器建立tcp连接
            TcpClient tcpClient;
            StateObject stateObject;
            tcpClient = new TcpClient();
            stateObject = new StateObject();
            stateObject.tcpClient = tcpClient;
            stateObject.buffer = Send;
            int p = int.Parse(port);
            tcpClient.BeginConnect(ip, p, new AsyncCallback(SendCallbackF), stateObject);
        }

        //注册登录状况
        public void state(string s)
        {
            int i = s.IndexOf("##");
            string s1 = s.Substring(0, i);
            string s2 = s.Substring(i + 2, s.Length - i - 2);
            infortextBlock.Text = s1;
            if (s2 == "0")
            {
                this.Close();
            }
        }
    }
}
