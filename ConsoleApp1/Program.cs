﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Runtime.Remoting.Messaging;



/// <summary>
/// 作为IM即时通讯的服务器
/// 接收登录注册信息，并返回结果
/// 在数据库读取/写入用户数据
/// 返回客户端在线用户数据
/// 
/// 消息定义
/// 下线广播  Offline##name
/// 上线广播  Online##ip:port#name
/// 登录成功  Login##1
/// 登录失败  Login##0
/// 注册成功  Register##1
/// 注册失败  Register##0
/// </summary>
namespace Service
{
    class Program
    {
        static int MyPort = 1499;  //本机端口号
        IPAddress MyIPAdress = null;  //本机ip地址
        TcpListener tcpListener = null;  //监听
        string IPAndPort;  //本机ip与端口号
        private Thread Listenerthread;

        private delegate void ReadDataF(TcpClient tcpClient);
        private delegate void LoginF(string infor);
        private delegate void RegisterF(string infor);
        private delegate void OfflineF(string infor);
        private delegate void OnlineF(string infor);



        //自定义类--建立发送信息连接所需要的信息（客户端-流（内容）-服务端）
        public class StateObject
        {
            public TcpClient tcpClient = null;  //tcp客户端
            public NetworkStream networkStream = null;  //发送信息流
            public byte[] buffer;  //发送信息-字节序列
            public string name = null;  //用户信息
        }

        //查找可用端口
        public bool FindPort()
        {
            MyIPAdress = (IPAddress)Dns.GetHostAddresses(Dns.GetHostName()).Where(ip => ip.AddressFamily == AddressFamily.InterNetwork).Last();
            //MyIPAdress = (IPAddress)Dns.GetHostAddresses(Dns.GetHostName())[0];
            MyPort++;
            for (int i = 0; i < 51; i++)
            {
                //端口尝试
                try
                {
                    Console.WriteLine("正在尝试连接...第{0}次", i + 1);
                    tcpListener = new TcpListener(MyIPAdress, MyPort);
                    tcpListener.Start();
                    IPAndPort = MyIPAdress.ToString() + ":" + MyPort.ToString();
                    Console.WriteLine("连接成功！服务器的本地ip与端口号为  " + IPAndPort);
                    break;
                }
                catch
                {
                    MyPort++;
                }
                if (i == 50)
                {
                    Console.WriteLine("连接失败！请检查您的网络连接！");
                    return false;
                }
            }
            return true;
        }

        //线程方法
        public void ListenerthreadMethod()
        {
            TcpClient tcpClient = null;
            ReadDataF readDataF = new ReadDataF(readRevMsg);
            while (true)
            {
                try
                {
                    //同步阻塞
                    tcpClient = tcpListener.AcceptTcpClient();
                    //异步调用
                    readDataF.BeginInvoke(tcpClient, null, null);
                }
                catch { }
            }
        }

        //接收到信息后的操作
        public void readRevMsg(TcpClient tcpClient)
        {
            //从流中读取数据并转为string
            byte[] bytes = ReadFromTcpClient(tcpClient);
            UnicodeEncoding ascii = new UnicodeEncoding();
            string s = ascii.GetString(bytes);
            //信息解析
            int i1 = s.IndexOf("##");
            int i2 = s.IndexOf("##", i1 + 1);
            string title = s.Substring(0, i1);  //消息头
            string infor = s.Substring(i1 + 2, i2 - i1 - 2);  //发送者信息
            string content = s.Substring(i2 +2, s.Length - i2 - 2);  //接收信息内容
            i1 = infor.IndexOf("#");
            i2 = infor.IndexOf("#", i1 + 1);
            switch (title)
            {
                case "Login":
                    Console.WriteLine(infor.Substring(0,i2) + "正在尝试登录...");
                    LoginF loginF = new LoginF(login);
                    loginF.BeginInvoke(infor, null, null);
                    break;
                case "Register":
                    Console.WriteLine(infor.Substring(0, i2) + "正在尝试注册...");
                    RegisterF registerF = new RegisterF(register);
                    registerF.BeginInvoke(infor, null, null);
                    break;
                case "Offline":
                    Console.WriteLine(infor.Substring(0, i2) + "正在尝试下线...");
                    OfflineF offlineF = new OfflineF(offline);
                    offlineF.BeginInvoke(infor, null, null);
                    break;
                case "Read":
                    Console.WriteLine(infor.Substring(0, i2) + "正在尝试读取数据库在线用户...");
                    OnlineF onlineF = new OnlineF(online);
                    onlineF.BeginInvoke(infor, null, null);
                    break;

            }

            //在聊天界面显示
            //this.Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, new OneArgDelegate(SetFriendListBox), s);
        }

        //异步登录操作
        public void login(string infor)
        {
            int i1 = infor.IndexOf("#");
            int i2 = infor.IndexOf("#", i1 + 1);
            string FriendIPAndPort = infor.Substring(0, i1);  //发送者的IP和port
            string Name = infor.Substring(i1 + 1, i2 - i1 - 1);  //发送者的名字
            string Password = infor.Substring(i2+1, infor.Length - i2 - 1);  //发送者的密码
            string dataaddress = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\C#\Code\IM\IMUserDataMis.mdf;Integrated Security=True;Connect Timeout=30";
            SqlConnection conn;
            SqlCommand da;
            conn = new SqlConnection(dataaddress);
            //登录验证
            string t = "select * from UserData where UserName='" + Name + "' and UserPassword='" + Password + "'";
            conn.Open();
            da = new SqlCommand(t, conn);  //连接数据库
            SqlDataReader dr = da.ExecuteReader();
            //登录成功
            if (dr.Read())
            {
                //不关闭后面改变da指向报错
                dr.Close();
                //将要发送的string转为字节序列
                string s = "Login##1";  //登录成功
                UnicodeEncoding ascii = new UnicodeEncoding();
                int n = (ascii.GetBytes(s)).Length;
                byte[] Send = new byte[n];
                Send = ascii.GetBytes(s);
                //改变数据库中的用户登录状态
                t = "Update UserData Set UserState = '1',UserIPAndPort='"+ FriendIPAndPort+" 'Where UserName ='" + Name + "'";
                da = new SqlCommand(t, conn);
                da.ExecuteNonQuery();
                conn.Close();
                //建立tcp连接
                string ip = null;
                int port = 0;
                TcpClient tcpClient = new TcpClient();
                StateObject stateObject = new StateObject();
                stateObject.tcpClient = tcpClient;
                stateObject.buffer = Send;
                int i4 = FriendIPAndPort.IndexOf(":");
                //PAddress ip = IPAddress.Parse(IPAndPort.Substring(0, i4));
                ip = FriendIPAndPort.Substring(0, i4);
                port = int.Parse(FriendIPAndPort.Substring(i4 + 1, FriendIPAndPort.Length - i4 - 1));
                stateObject.name = ip + ":" + port.ToString() + "#" + Name;
                //回复登录成功
                tcpClient.BeginConnect(ip, port, new AsyncCallback(SendCallbackF), stateObject);
                Console.WriteLine(stateObject.name + "登录成功！");
            }
            //登录失败
            else
            {
                dr.Close();
                //将要发送的string转为字节序列
                string s = "Login##0";  //登录失败
                UnicodeEncoding ascii = new UnicodeEncoding();
                int n = (ascii.GetBytes(s)).Length;
                byte[] Send = new byte[n];
                Send = ascii.GetBytes(s);
                //建立tcp连接
                string ip = null;
                int port = 0;
                TcpClient tcpClient = new TcpClient();
                StateObject stateObject = new StateObject();
                stateObject.tcpClient = tcpClient;
                stateObject.buffer = Send;
                int i4 = FriendIPAndPort.IndexOf(":");
                //PAddress ip = IPAddress.Parse(IPAndPort.Substring(0, i4));
                ip = FriendIPAndPort.Substring(0, i4);
                port = int.Parse(FriendIPAndPort.Substring(i4 + 1, FriendIPAndPort.Length - i4 - 1));
                stateObject.name = ip + ":" + port.ToString() + "#" + Name;
                tcpClient.BeginConnect(ip, port, new AsyncCallback(SendCallbackF), stateObject);
                Console.WriteLine(stateObject.name + "登录失败！");
            }
            conn.Close();
        }

        //发送消息回调函数
        public void SendCallbackF(IAsyncResult ar)
        {
            StateObject stateObject = (StateObject)ar.AsyncState;
            TcpClient tcpClient = stateObject.tcpClient;
            NetworkStream networkStream = null;
            //尝试写数据入流
            try
            {
                tcpClient.EndConnect(ar);
                networkStream = tcpClient.GetStream();
                //把数据写入networkStream
                if (networkStream.CanWrite) networkStream.Write(stateObject.buffer, 0, stateObject.buffer.Length);
                else Console.WriteLine("发送给" + stateObject.name + "的登录消息失败");
            }
            catch
            {
                Console.WriteLine("发送给" + stateObject.name + "的登录消息失败");
            }
            finally
            {
                if (networkStream != null) networkStream.Close();
                tcpClient.Close();
            }
        }

        //异步注册操作
        public void register(string infor)
        {
            int i1 = infor.IndexOf("#");
            int i2 = infor.IndexOf("#", i1 + 1);
            string FriendIPAndPort = infor.Substring(0, i1);  //发送者的IP和port
            string Name = infor.Substring(i1 + 1, i2 - i1 - 1);  //发送者的名字
            string Password = infor.Substring(i2 + 1, infor.Length - i2 - 1);  //发送者的密码
            string dataaddress = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\C#\Code\IM\IMUserDataMis.mdf;Integrated Security=True;Connect Timeout=30";
            SqlConnection conn;
            SqlCommand da;
            conn = new SqlConnection(dataaddress);
            //解决插入中文字符时会显示？？？（已解决）
            //参考https://jingyan.baidu.com/article/c74d600060804a0f6a595dfd.html
            //参考https://www.cnblogs.com/FocusIN/p/5392765.html
            string t = "Insert Into UserData(UserName,UserPassword,UserIPAndPort,UserState) Values('";
            t += Name + "','" + Password + "','"+ FriendIPAndPort + "','"+"0" + "')";
            da = new SqlCommand(t, conn);  //进行数据库操作
            conn.Open();
            string s = null;
            StateObject stateObject = new StateObject();
            stateObject.name = FriendIPAndPort + "#" + Name;
            try
            {
                da.ExecuteNonQuery();
                s = "Register##1";  //注册成功
                Console.WriteLine(stateObject.name + "注册成功！");
            }
            catch {
                s = "Register##0";  //注册失败
                Console.WriteLine(stateObject.name + "注册失败！");
            }
            finally
            {
                conn.Close();   //记得关闭数据库连接
                //将要发送的string转为字节序列
                UnicodeEncoding ascii = new UnicodeEncoding();
                int n = (ascii.GetBytes(s)).Length;
                byte[] Send = new byte[n];
                Send = ascii.GetBytes(s);
                //建立tcp连接
                string ip = null;
                int port = 0;
                TcpClient tcpClient = new TcpClient();
                stateObject.tcpClient = tcpClient;
                stateObject.buffer = Send;
                int i4 = FriendIPAndPort.IndexOf(":");
                //PAddress ip = IPAddress.Parse(IPAndPort.Substring(0, i4));
                ip = FriendIPAndPort.Substring(0, i4);
                port = int.Parse(FriendIPAndPort.Substring(i4 + 1, FriendIPAndPort.Length - i4 - 1));
                tcpClient.BeginConnect(ip, port, new AsyncCallback(SendCallbackF), stateObject);
            }
        }

        //发送消息回调函数
        public void RegisterCallbackF(IAsyncResult ar)
        {
            StateObject stateObject = (StateObject)ar.AsyncState;
            TcpClient tcpClient = stateObject.tcpClient;
            NetworkStream networkStream = null;
            //尝试写数据入流
            try
            {
                tcpClient.EndConnect(ar);
                networkStream = tcpClient.GetStream();
                //把数据写入networkStream
                if (networkStream.CanWrite) networkStream.Write(stateObject.buffer, 0, stateObject.buffer.Length);
                else Console.WriteLine("发送给" + stateObject.name + "的注册回复消息失败");
            }
            catch
            {
                Console.WriteLine("发送给" + stateObject.name + "的注册回复消息失败");
            }
            finally
            {
                if (networkStream != null) networkStream.Close();
                tcpClient.Close();
            }
        }

        //异步下线操作
        public void offline(string infor)
        {
            //改变登录状态
            int i1 = infor.IndexOf("#");
            int i2 = infor.IndexOf("#", i1 + 1);
            string FriendIPAndPort = infor.Substring(0, i1);  //发送者的IP和port
            string Name = infor.Substring(i1 + 1, i2 - i1 - 1);  //发送者的名字
            string dataaddress = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\C#\Code\IM\IMUserDataMis.mdf;Integrated Security=True;Connect Timeout=30";
            SqlConnection conn;
            SqlCommand da;
            conn = new SqlConnection(dataaddress);
            conn.Open();
            string t = "Update UserData Set UserState = '0' Where UserName ='" + Name + "'";
            da = new SqlCommand(t, conn);
            da.ExecuteNonQuery();
            conn.Close();
            //更新在线好友
            string ip = null;
            int port = 0;
            int i4 = FriendIPAndPort.IndexOf(":");
            ip = FriendIPAndPort.Substring(0, i4);
            port = int.Parse(FriendIPAndPort.Substring(i4 + 1, FriendIPAndPort.Length - i4 - 1));
            OfflineUser(ip, port,Name);
        }

        //从TcpClient对象中读出未知长度的字节数组
        public byte[] ReadFromTcpClient(TcpClient tcpClient)
        {
            List<byte> data = new List<byte>();
            NetworkStream networkStream = null;
            //新建可储存缓冲区长度的字节数组
            byte[] bytes = new byte[tcpClient.ReceiveBufferSize];
            int n = 0;
            try
            {
                networkStream = tcpClient.GetStream();
                if (networkStream.CanRead)
                {
                    do
                    {
                        n = networkStream.Read(bytes, 0, (int)tcpClient.ReceiveBufferSize);
                        //若还未读到末尾
                        if (n == (int)tcpClient.ReceiveBufferSize) data.AddRange(bytes);
                        //读到结尾
                        else if (n != 0)
                        {
                            byte[] bytes1 = new byte[n];
                            for (int i = 0; i < n; i++)
                            {
                                bytes1[i] = bytes[i];
                            }
                            data.AddRange(bytes1);
                        }
                    } while (networkStream.DataAvailable);
                }
                bytes = data.ToArray();
            }
            catch
            {
                Console.WriteLine("读取数据失败！");
                bytes = null;
            }
            finally
            {
                if (networkStream != null) networkStream.Close();
                tcpClient.Close();
            }
            return bytes;
        }

        //异步返回在线用户
        public void online(string infor)
        {
            int i1 = infor.IndexOf("#");
            int i2 = infor.IndexOf("#", i1 + 1);
            string FriendIPAndPort = infor.Substring(0, i1);  //发送者的IP和port
            string name = infor.Substring(i1 + 1, i2 - i1 - 1);  //发送者的名字
            int i3 = FriendIPAndPort.IndexOf(":");
            //PAddress ip = IPAddress.Parse(IPAndPort.Substring(0, i4));
            string ip = FriendIPAndPort.Substring(0, i3);
            int port = int.Parse(FriendIPAndPort.Substring(i3 + 1, FriendIPAndPort.Length - i3 - 1));

            string s, s1;
            string r;
            UnicodeEncoding ascii = new UnicodeEncoding();
            StateObject stateObject, stateObject1;
            TcpClient tcpClient, tcpClient1;
            string friendip = null;
            int friendport = 0;
            int i4;
            string dataaddress = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\C#\Code\IM\IMUserDataMis.mdf;Integrated Security=True;Connect Timeout=30";
            SqlConnection conn;
            SqlCommand da;
            conn = new SqlConnection(dataaddress);
            conn.Open();
            string t = "select * from UserData where UserState = '1'";
            da = new SqlCommand(t, conn);
            SqlDataReader dr = da.ExecuteReader();
            while (dr.Read())
            {
                s = "Online##";  //在线用户
                stateObject = new StateObject();
                stateObject1 = new StateObject();
                s1 = s + ip + ":" + port.ToString();
                r = dr.GetSqlString(2).ToString();
                i4 = r.IndexOf(":");
                friendip = r.Substring(0, i4);
                friendport = int.Parse(r.Substring(i4 + 1, r.Length - i4 - 1));
                r += "#";
                r += dr.GetSqlString(0).ToString();
                s += r;
                s1 = s1 + "#" + name;
                stateObject.name = r;
                stateObject1.name = s1 + " TO " + friendip + ":" + friendport;
                ascii = new UnicodeEncoding();
                int n = (ascii.GetBytes(s)).Length;
                byte[] Send = new byte[n];
                Send = ascii.GetBytes(s);
                tcpClient = new TcpClient();
                tcpClient1 = new TcpClient();
                stateObject.tcpClient = tcpClient;
                stateObject.buffer = Send;
                tcpClient.BeginConnect(ip, port, new AsyncCallback(ToUserCallbackF), stateObject);
                n = (ascii.GetBytes(s1)).Length;
                Send = new byte[n];
                Send = ascii.GetBytes(s1);
                stateObject1.tcpClient = tcpClient1;
                stateObject1.buffer = Send;
                tcpClient1.BeginConnect(friendip, friendport, new AsyncCallback(ToUserCallbackF), stateObject1);
            }
            dr.Close();
            conn.Close();
        }

        //广播用户下线
        public void OfflineUser(string ip,int port,string name)
        {
            string s = "Offline##" + name;
            string r;
            UnicodeEncoding ascii = new UnicodeEncoding();
            StateObject stateObject;
            TcpClient tcpClient;
            string friendip = null;
            int friendport = 0;
            int i4;
            string dataaddress = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\C#\Code\IM\IMUserDataMis.mdf;Integrated Security=True;Connect Timeout=30";
            SqlConnection conn;
            SqlCommand da;
            conn = new SqlConnection(dataaddress);
            conn.Open();
            string t = "select * from UserData where UserState = '1'";
            da = new SqlCommand(t, conn);
            SqlDataReader dr = da.ExecuteReader();
            while (dr.Read())
            {
                stateObject = new StateObject();
                r = dr.GetSqlString(2).ToString(); //ipandport
                i4 = r.IndexOf(":");
                friendip = r.Substring(0, i4);
                friendport = int.Parse(r.Substring(i4 + 1, r.Length - i4 - 1));
                r += dr.GetSqlString(0).ToString();
                r = s + " TO " + r;
                stateObject.name = r;
                ascii = new UnicodeEncoding();
                int n = (ascii.GetBytes(s)).Length;
                byte[] Send = new byte[n];
                Send = ascii.GetBytes(s);
                tcpClient = new TcpClient();
                stateObject.tcpClient = tcpClient;
                stateObject.buffer = Send;
                tcpClient.BeginConnect(friendip, friendport, new AsyncCallback(ToUserCallbackF), stateObject);
            }
            dr.Close();
            conn.Close();
        }

        //发送消息回调函数
        public void ToUserCallbackF(IAsyncResult ar)
        {
            StateObject stateObject = (StateObject)ar.AsyncState;
            TcpClient tcpClient = stateObject.tcpClient;
            NetworkStream networkStream = null;
            //尝试写数据入流
            try
            {
                tcpClient.EndConnect(ar);
                networkStream = tcpClient.GetStream();
                //把数据写入networkStream
                if (networkStream.CanWrite) networkStream.Write(stateObject.buffer, 0, stateObject.buffer.Length);
                else Console.WriteLine("发送给用户" + stateObject.name + "广播消息失败");
            }
            catch
            {
                Console.WriteLine("发送给用户" + stateObject.name + "广播消息失败");
            }
            finally
            {
                if (networkStream != null) networkStream.Close();
                tcpClient.Close();
            }
        }


        static void Main(string[] args)
        {
            Program p = new Program();
            if (!p.FindPort()) return;
            p.Listenerthread = new Thread(new ThreadStart(p.ListenerthreadMethod));
            p.Listenerthread.IsBackground = true;
            p.Listenerthread.Start();
            while (true) { }
        }
    }
}
